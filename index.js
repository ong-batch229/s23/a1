let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function() {
		console.log("Pikachu! I choose you!")
	}
}

console.log(trainer)

console.log("Result of dot notation:")
console.log(trainer.name)
console.log(trainer.age)

console.log("Result of square bracket notation:")
console.log(trainer.pokemon)
console.log(trainer.friends)

console.log("Result of talk() method:")
trainer.talk()

function Pokemon(name, level) {
	this.name = name
	this.level = level
	this.health = level * 3
	this.attack = level * 1.5

	this.tackle = function(targetPokemon) {
		console.log(this.name + " tackled " + targetPokemon.name)
	}

	this.faint = function() {
		console.log(this.name + " fainted!")
	}
}

let pokemon1 = new Pokemon("Pikachu", 12)
console.log(pokemon1)

let pokemon2 = new Pokemon("Geodude", 8)
console.log(pokemon2)

let pokemon3 = new Pokemon("Mewtwo", 100)
console.log(pokemon3)

pokemon2.tackle(pokemon1)
pokemon3.tackle(pokemon2)
pokemon3.faint();